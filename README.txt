Description
-----------
Add tickets to events without the need to go to the 
products dashboard to add products.

Requirements
------------
Drupal 7.x
Drupal Commerce
Views
Entity

Installation
-----------
1. Enable the module
2. Add field to the node. Select "Product Reference" as 
field, and "Commerce Event Ticket" as Widget".
3. On the Edit Field page, look for "Product types that can be referenced". 
Select "Commerce Event Ticket".
